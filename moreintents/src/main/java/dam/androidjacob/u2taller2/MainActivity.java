package dam.androidjacob.u2taller2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.actions.NoteIntents;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String IMPLICIT_INTENTS = "ImplicitIntents";
    private static EditText etContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    public void setUI() {
        Button btContact, btWifi, btCamera;

        etContact = findViewById(R.id.etContact);

        btContact = findViewById(R.id.buttonContact);
        btWifi = findViewById(R.id.buttonWifi);
        btCamera = findViewById(R.id.buttonCamera);

        btContact.setOnClickListener(this);
        btWifi.setOnClickListener(this);
        btCamera.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonContact:
                createContact(etContact.getText().toString());
                break;
            case R.id.buttonWifi:
                openWifi();
                break;
            case R.id.buttonCamera:
                openCamera();
                break;

        }
    }

    private void createContact(String name) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
        if (intent.resolveActivity(getPackageManager()) != null) {
            Log.d(IMPLICIT_INTENTS, "createContact: App found, Executing");
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "createContact: Can't handle this intent!");
        }
    }

    private void openWifi() {
        Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
        if (intent.resolveActivity(getPackageManager()) != null) {
            Log.d(IMPLICIT_INTENTS, "openWifi: App found, Executing");
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "openWifi: Can't handle this intent!");
        }
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
        if (intent.resolveActivity(getPackageManager()) != null) {
            Log.d(IMPLICIT_INTENTS, "openCamera: App found, Executing");
            startActivity(intent);
        } else {
            Log.d(IMPLICIT_INTENTS, "openCamera: Can't handle this intent!");
        }
    }
}