package dam.androidjacob.u2taller2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String IMPLICIT_INTENTS = "ImplicitIntents";
    private static EditText etUri, etLocation, etText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    public void setUI(){
        Button btUri, btLocation, btText;

        etUri = findViewById(R.id.editTextURI);
        etLocation = findViewById(R.id.editTextMaps);
        etText = findViewById(R.id.editTextShare);

        btUri = findViewById(R.id.buttonURI);
        btLocation = findViewById(R.id.buttonMaps);
        btText = findViewById(R.id.buttonShare);

        btUri.setOnClickListener(this);
        btLocation.setOnClickListener(this);
        btText.setOnClickListener(this);
    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.buttonURI:
                openWebsite(etUri.getText().toString());
                break;
            case R.id.buttonMaps:
                openLocation(etLocation.getText().toString());
                break;
            case R.id.buttonShare:
                shareText(etText.getText().toString());
        }
    }

    private void openWebsite(String urlText){
        Uri webpage = Uri.parse(urlText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d(IMPLICIT_INTENTS, "openWebsite: Can't handle this intent!");
        }
    }

    private void openLocation(String location){
        Uri addressUri = Uri.parse("geo:0,0'q=" + location);
        Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);

        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d(IMPLICIT_INTENTS, "openLocation: Can't handle this intent!");
        }
    }

    private void shareText(String text){
        new ShareCompat.IntentBuilder(this)
                .setType("text/plain")
                .setText(text)
                .startChooser();
        }
    }